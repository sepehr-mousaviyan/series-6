package sbu.cs.multithread.pi;

public class PICalculator {

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */
    public String calculate(int floatingPoint) throws InterruptedException {
        Calculate calculate = new Calculate(floatingPoint+2);

        String Pi = calculate.compute().toString();
        String answer = new StringBuilder(Pi).deleteCharAt(Pi.length() - 1).toString();
        return answer;
    }
}
