package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Calculate {
    private int numDigits;
    private MathContext mathContext;
    private BigDecimal pi = new BigDecimal(0);

    public Calculate(int numDigits) {
        this.numDigits = numDigits;
        mathContext = new MathContext(numDigits);
    }

    public BigDecimal compute() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(numDigits);
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        for (int k = 0; k < numDigits; k++) {
            int counter = k;
            Thread thread = new Thread(() -> {
                synchronized ("") {
                    BigDecimal pi_K = piFunction(counter);
                    pi = pi.add(pi_K);
                }
                countDownLatch.countDown();
            });
            executorService.submit(thread);
        }
        executorService.shutdown();
        countDownLatch.await();
        BigDecimal answer = pi.round(mathContext);

        return answer;
    }

    private BigDecimal piFunction(int k) {
        int k8 = k * 8;
        BigDecimal value1 = new BigDecimal(4);
        value1 = value1.divide(new BigDecimal(k8 + 1), mathContext);
        BigDecimal value2 = new BigDecimal( -2);
        value2 = value2.divide(new BigDecimal(k8 + 4), mathContext);
        BigDecimal value3 = new BigDecimal(-1);
        value3 = value3.divide(new BigDecimal(k8 + 5), mathContext);
        BigDecimal value4 = new BigDecimal(-1);
        value4 = value4.divide(new BigDecimal(k8 + 6), mathContext);

        BigDecimal value = value1;
        value = value.add(value2);
        value = value.add(value3);
        value = value.add(value4);

        BigDecimal multiplier = new BigDecimal(16);
        multiplier = multiplier.pow(k);
        BigDecimal one = new BigDecimal(1);
        multiplier = one.divide(multiplier, mathContext);
        value = value.multiply(multiplier);

        return value;
    }
}
