package sbu.cs.multithread.semaphor;
import java.util.concurrent.Semaphore;

public class Chef extends Thread {

    public Chef(String name) {
        super(name);
    }

    @Override
    public void run() {
        Semaphore semaphore = new Semaphore(2);

        for (int i = 0; i < 10; i++) {
            Source.getSource(semaphore);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
