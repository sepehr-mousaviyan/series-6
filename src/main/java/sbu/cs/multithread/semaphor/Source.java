package sbu.cs.multithread.semaphor;
import java.util.concurrent.Semaphore;

public class Source {

    public static void getSource(Semaphore semaphore) {
        // some process that is limited to 2 persons concurrently
        try {
            semaphore.acquire();
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        semaphore.release();
    }
}
